Titre
Xavier Cartron
Dernière mise à jour: %%mtime(%d/%m/%Y)

%! encoding: utf-8
%!options: --toc --toc-level 4


===Avec symon===[symon]

Symon va vous permettre d'obtenir des statistiques sur les ressources utilisées
par votre serveur et de les
consulter sur une page web.

Commençons par installer le nécessaire : 

```
# pkg_add symon symux syweb
```

On modifie ensuite la configuration de symon qui va surveiller le système. Pour
cela, on modifie le fichier ``/etc/symon.conf`` et on garde les informations qui
nous intéressent : 

```
monitor { cpu(0),  mem,
          if(lo0),
          if(re0),
          pf,
          mbuf,
          sensor(cpu0.temp0),
          proc(httpd),
          io(wd0),
          io(wd1),
          df(sd1b)
} stream to 127.0.0.1 2100

```


- ``cpu(0)`` : On surveille la charge du processeur,
- ``mem`` : On surveille la mémoire,
- ``if(re0)`` : On surveille l'interface web re0,
- ``pf`` : On surveille le parefeu,
- ``mbuf`` : On surveille la mémoire cache,
- ``sensor(cpu0.temp0)`` : On surveille la température du processeur,
- ``proc(httpd)`` : On surveille l'activité du démon httpd,
- ``io(wd0)`` : On regarde l'activité sur le disque wd0,
- ``df(sd1b)`` : On surveille l'espace restant sur la partition sd1b.


De la même façon, on configure symux qui va analyser les données recueillies par
symon. On édite le fichier ``/etc/symux.conf`` : 


```
mux 127.0.0.1 2100

source 127.0.0.1 {
	accept { cpu(0),  mem,
		if(lo0),
		if(re0),
		pf,
		mbuf,
		sensor(cpu0.temp0),
		proc(httpd),
		io(wd0)
		io(wd1),
		df(sd1b)
	}

	datadir "/var/www/symon/rrds/localhost"
}
```


On ne fait que reproduire les mêmes éléments que dans ``/etc/symon.conf``.
Attention à bien indiquer le chemin vers les données ``datadir``.
On va d'ailleurs le créer dès maintenant : 

```
# mkdir -p -m 0755 /var/www/symon/rrds/localhost
```

On crée maintenant les fichiers rrd : 

```
# /usr/local/share/examples/symon/c_smrrds.sh all
```

Puisque le serveur web est dans un chroot, il faut lancer la commande suivante
pour installer l'outil d'analyse des données (rddtool) : 

```
# /usr/local/share/examples/rrdtool/rrdtool-chroot enable
```

Une fois ceci fait, il faut préciser dans la configuration de syweb où trouver
l'outil rrdtool. Dans le fichier ``/var/www/htdocs/syweb/setup.inc``,
modifiez la ligne contenant ``$symon['rrdtool_path']`` ainsi : 

```
$symon['rrdtool_path']='/usr/local/bin/rrdtool';
```


Reste une dernière modification à apporter. Puisque le serveur web est en
chroot, il faut lui donner accès au shell ``/bin/sh`` afin que PHP puisse lancer
rrdtool qui génère les graphiques : 

```
# mkdir -p /var/www/bin
# cp /bin/sh /var/www/bin
```


On recharge tous les nouveaux démons après les avoir activés : 

```
# rcctl enable symon
# rcctl enable symux
# rcctl start symon
# rcctl start symux
```


On ajoute la configuration convenable pour le serveur [http #httpd], dans le
fichier ``/etc/httpd.conf`` : 


```
server "statistiques.&NDD" {
        listen on * port 80  
        root "/htdocs/syweb"
        directory index index.php

        location "*.php*" {
                fastcgi socket "/run/php-fpm.sock"
        }
}
```

Reste à recharger ce dernier avec ``# rcctl reload httpd`` puis à aller sur le
site définit pour voir les beaux graphiques.


[img/symon.png]

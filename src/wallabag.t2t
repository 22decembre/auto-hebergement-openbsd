Titre
Xavier Cartron
Dernière mise à jour: %%mtime(%d/%m/%Y)

%! encoding: utf-8
%!options: --toc --toc-level 4

===Wallabag===
[Wallabag https://wallabag.org/] permet de sauvegarder des pages web pour les
lire plus tard. On le décrit comme une application de "read-it-later". Voici comment l'installer.


On récupère wallabag dans le dossier ``/tmp`` :

```
$ cd /tmp
$ ftp https://wllbg.org/latest-v2-package && tar xvzf latest-v2-package
```

On installe wallabag dans ``/var/www/htdocs`` : 

```
# mv /tmp/release* /var/www/htdocs/wallabag
# chown -R www:daemon /var/www/htdocs/wallabag
```


On peut désormais éditer le fichier ``/etc/httpd.conf`` afin d'ajouter une
section pour wallabag : 

```
server "bag.&NDD" {
    listen on * port 80
    block return 301 "https://$SERVER_NAME$REQUEST_URI"
}

server "bag.&NDD" {
    listen on * tls port 443
    root "/htdocs/wallabag/web" 
    directory index app.php
    hsts
    tls {
        &SSLCERT
        &SSLKEY
    }

# on bloque tous les scripts php sauf celui utile
    location "/*.php"          { block }

# on reformate l'URL si besoin
    location match "^/[^(app.php)]/(.*)$" {
        block return 301 "https://$SERVER_NAME/app.php/%1"
    }
    location "/" {
        block return 301 "https://$SERVER_NAME/app.php/"
    }

# On sert les pages
    location "/app.php/*" {
        fastcgi socket "/run/php-fpm.sock"
    }
}
```


Il ne vous reste plus qu'à relancer httpd puis à aller sur votre site. Les
identifiants par défaut sont ``wallabag``/``wallabag``. Créez un nouveau compte ou
modifiez ce dernier pour terminer l'installation.
